"use client"

import Image from "next/image";
import styles from "./page.module.css";
import Header from "./componets/Header";
import Service from "./componets/Service";
import Reference from "./componets/Reference";
import Care from "./componets/Care";
import Footer from "./componets/Footer";
import Button from '@mui/material/Button';
import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionActions from '@mui/material/AccordionActions';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Box, CardActionArea, CardActions, Grid } from "@mui/material";
import CardService from "./componets/CardServicie";
import { Loading } from "./componets/Loading";
import { ChevronRight } from "@mui/icons-material";
import { Console } from "console";
import { usePokemon } from "./hook/usePokemon";

export default function Home() {
  const {pokemon,loading} = usePokemon()
  console.log(loading? "cargando...":"")
  if(!loading){
    console.log(pokemon)
  }

    return (
    <main>
      <div>
      <Header title={"Bring Serenity to Your Place With Interior Plants"} subtitle={"find your dream plant for you home decoration with us, and we will make it happen."} image={""}></Header>
      <div className="fondo">
      <Image src="/imagenes/imga1.svg" alt="" width={2350} height={1410}></Image>
      </div>
      <div>
      <div className="links">
                <a href="#">Home</a>
                <a href="#">Shop</a>
                <a href="#">About Us</a>
                <a href="#">Contact</a>
            </div>
      </div>
      </div>
      <div className="buscar">
      <input type="text" placeholder="Serach plant"  ></input>/
      </div>
      <div className="buscador">
        <Image src="/imagenes/icon.svg" alt="" width={50} height={45.57}></Image>
      </div>
      <div className="icono">
        <Image src="/imagenes/icon2.svg" alt="" width={32} height={36.57}></Image>
      </div>
      <div>
        <Image src="imagenes/imagen1.svg" alt="" width={205} height={116} className="imagen1"/>
        <Image src="imagenes/imagen2.svg" alt="" width={54} height={54} className="imagen2"/>
        <Image src="imagenes/imagen2.svg" alt="" width={54} height={54} className="imagen3"/>
        <Image src="imagenes/imagen2.svg" alt="" width={54} height={54} className="imagen4"/>      
      </div>
      <div className="services">
      <Service title={"Free Shapping"} subtitle={"No charge for each delivery"} image={"imagenes/icon3.svg"}></Service>
      <Service title={"Quick Payment"} subtitle={"100% secure payment"} image={"imagenes/icon4.svg"}></Service>
      <Service title={"24/7 Support"} subtitle={"Quick support"} image={"imagenes/icon5.svg"}></Service>
      </div>
      <section className="card">
        <Reference title={"Interior Plant Reference"} subtitle={"make your home so comfortable with refreshing plants"} image={""}></Reference>
        <div className="ref">
        <Image src="imagenes/refence.svg" alt="" width={735} height={549} className="ref1"></Image>
        <Image src="imagenes/refence1.svg" alt="" width={734} height={404} className="ref2"></Image>
        <Image src="imagenes/refence2.svg" alt="" width={717} height={995} className="ref3"></Image>
        </div>
      </section>
      <section className="care">
        <Care title={"How to care for plants"} subtitle={"Take care of plants with all your heart"} image={""}></Care>
      </section>
      <div className="care2">
        <Care title={"Adjust Lighting"} subtitle={"When caring for indoor plants, make sure the room temperature is neither too cold nor too hot"} image={"imagenes/icon6.svg"}></Care>
        <Care title={"Don't water too often"} subtitle={"Watering ornamental plants indoors does not have to be done every day."} image={"imagenes/icon7.svg"}></Care>
        <Care title={"Don't water too often"} subtitle={"Watering ornamental plants indoors does not have to be done every day."} image={"imagenes/icon8.svg"}></Care>
        <Care title={"Fertilize regularly"} subtitle={"The nutrients most indoor houseplants need are nitrogen for balance and potassium for stem strength"} image={"imagenes/icon9.svg"}></Care>
        </div>
        <section>
        <div>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1-content"
          id="panel1-header"
        >
          Accordion 1
        </AccordionSummary>
        <AccordionDetails>
          <Box sx={{display:"flex",paddingBlock:"50px"}}>
          {pokemon?.results.map((item: any, index: number) => {
                const splitUrl = item.url.split('/')
                const id = splitUrl[splitUrl.length - 2]
                return (
                  
                    <CardService 
                    title={item.name}
                    image={`${process.env.NEXT_PUBLIC_IMAGES_POKEMON}/${id}.png`}
                    name='Ethiopian runners took the top four spots.'
                    date='Jan 10, 2022 ∙ 3 min read' avatar={""} >
                    </CardService >
                  
                )
              })}
          </Box>
        </AccordionDetails>
      </Accordion>
    </div>
        </section>
        <section className="fondo2">
          <div className="tib">
          <Footer title={"Newslatter"} subtitle={""} subtitle2={""} subtitle3={""} ></Footer>
          <div  className="buscar2">
            <label></label>
          <input type="email" placeholder="Enter your email"></input>
          </div>
          </div>
          <div className="footer1">
            <Footer title={"Support"} subtitle={"About Us"} subtitle2={"Careers"} subtitle3={"Blog"}></Footer>
            <Footer title={"Useful Link"} subtitle={"Payment & Tax"} subtitle2={"Team of service"} subtitle3={"Privaci Policy"}></Footer>
            <Footer title={"Our Menu"} subtitle={"Best Product"} subtitle2={"Category"} subtitle3={""}></Footer>
             <Footer title={"Address"} subtitle={"JL. Setiabudhi No. 193 Sukasari, BandungWest Java, Indonesia"} subtitle2={"hallo@daunku.com"} subtitle3={""}></Footer>
           </div>
           <Button className="suscribe" variant="outlined">Subscribe</Button>
           <hr></hr>
           <div className="end">
           <Footer title={"© 2023 davixq - All rights reserved."} subtitle={""} subtitle2={""} subtitle3={""}></Footer>
           </div>
           
        </section>
    </main>
  );
}


/*{pokemon?.results.map((item: any, index: number) => {
  const splitUrl = item.url.split('/')
  const id = splitUrl[splitUrl.length - 2]
  return (
<CardService avatar='A'
        title={item.name}
        image={`${process.env.NEXT_PUBLIC_IMAGES_POKEMON}/${id}.png`}
        name='David'
        date='Jan 10, 2022 ∙ 3 min read'></CardService> 
      )
    })}   */
/*<img src="imagenes/imagen1.svg"></img>
            <div className="links">
                <a href="#">Home</a>
                <a href="#">Shop</a>
                <a href="#">About Us</a>
                <a href="#">Contact</a>
            </div>
            <div>
                <h1 className="titulo">Bring Serenity to Your Place </h1>
                <h1 className="titulo2">With Interior Plants</h1>
                <p className="p">find your dream plant for you home decoration </p>
                <p className="p2">with us, and we will make it happen.</p>
                <div>
                    <input type="text" placeholder="Serach plant" className="buscador" ></input>/
                </div>
                <div className="icon">
                    <img src="imagenes/icon.svg"></img>
                </div>
                <div className="icon2">
                    <img src="imagenes/icon2.svg"></img>
                </div>
                <div className="imagen2">
                    <img src="imagenes/imagen2.svg"></img>
                </div>
                <div className="img1">
                    <img src="imagenes/img1.svg"></img>
                </div>
                <div className="img2">
                    <img src="imagenes/img1.svg"></img>
                </div>
            </div>*/

            /*<div className="icon3">
            <h5>Free Shapping</h5>
            <p>No charge for each delivery</p>
            <img src="imagenes/icon3.svg"></img>
            <div className="icon4">
            <h5>Free Shapping</h5>
            <p>No charge for each delivery</p>
            <img src="imagenes/icon4.svg"></img>
            </div>
            <div className="icon5">
            <h5>Free Shapping</h5>
            <p>No charge for each delivery</p>
            <img src="imagenes/icon5.svg"></img>
            </div>
    
        </div>*/