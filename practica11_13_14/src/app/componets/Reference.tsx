import Image from "next/image";

export default function Reference(props:{title:string;subtitle:string;image:string}){
    return(
        <div>
            <h1 className="referencia">{props.title}</h1>
            <p className="referen">{props.subtitle}</p>
            <div>
            <Image src={props.image} alt="" width={1} height={1} ></Image>
            </div>
        </div>
    )
}