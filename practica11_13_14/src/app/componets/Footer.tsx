export default function Footer(props:{title:string;subtitle:string;subtitle2:string;subtitle3:string}){
    return(
        <div>
            <h1>{props.title}</h1>
            <p className="f1">{props.subtitle} </p>
            <p className="f2">{props.subtitle2} </p>
            <p className="f3">{props.subtitle3} </p>
        </div>
    )
}