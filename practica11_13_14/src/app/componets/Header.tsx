import Image from "next/image";

export default function Header(props:{title:string;subtitle:string;image:string}){
    return(
       <div className="title">
        <h1 className="titulo">{props.title}</h1>
        <p className="subtitle">{props.subtitle}</p>
       <div>
       <Image src={props.image} alt="" width={1} height={1}></Image>
       </div>
       </div>   
    )
}