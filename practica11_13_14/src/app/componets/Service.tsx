import Image from "next/image";

export default function Service(props:{title:string;subtitle:string;image:string}){
    return(
        <div>
            <h3>{props.title}</h3>
            <p>{props.subtitle}</p>
            <Image src={props.image} alt="" height={100} width={100} className="iconos"></Image>
        </div>
    )
}