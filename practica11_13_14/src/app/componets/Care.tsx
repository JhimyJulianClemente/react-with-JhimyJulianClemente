import Image from "next/image";

export default function Care(props:{title:string;subtitle:string;image:string}){
    return(
        <div>
            <h1>{props.title}</h1>
            <p className="p">{props.subtitle}</p>
            <Image src={props.image} alt="" width={80} height={80} className="iconos2"></Image>
        </div>
    )
}