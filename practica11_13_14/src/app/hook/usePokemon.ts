
import { useEffect, useState } from "react"
import { getPokemon } from "../services/pokemon.services"

export const usePokemon =  () => {
    const [pokemon,setPokemon] = useState <any> ()
    const [loading,setLoading] = useState<boolean>(true)

    const fetchData = async () => {
        try{
            const responsePokemon = await getPokemon()
            setPokemon(responsePokemon)
        }catch(e){
            console.log(e) 
        }
        setLoading(false)
    }
    useEffect(() =>{
        fetchData()
    },[])

    return {pokemon,loading}
}